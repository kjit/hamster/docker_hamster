ARG HOST_UID=1000
ARG HOST_GID=1000

FROM ros:noetic-ros-base

ARG HOST_USER=test
ARG HOST_UID
ARG HOST_GID

SHELL ["/bin/bash", "-c"]

## Update and add dependencies
RUN apt-get update && \
	DEBIAN_FRONTEND=noninteractive apt install -y curl\ 
		dbus-x11 \
        git \
		htop \
		iproute2 \
		mc \
		nano \
		net-tools \
		snap \
		software-properties-common \
		telnet \
		terminator \
		vim \
		wget && \
	apt-get clean -qq && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* 


RUN useradd -m $HOST_USER && \
  echo "$HOST_USER:$HOST_USER" | chpasswd  && \
  usermod --shell /bin/bash $HOST_USER && \
  usermod -aG sudo $HOST_USER && \
  echo "$HOST_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$HOST_USER && \
  chmod 0440 /etc/sudoers.d/$HOST_USER && \
  usermod  --uid $HOST_UID $HOST_USER && \
  groupmod --gid $HOST_GID $HOST_USER

## Create workspace forROS(workspace)
RUN mkdir -p /workspace/src

# .............................................................................................
## Adding ROS packages
# Adding packages
RUN apt update && apt upgrade -y
RUN apt update && apt install -y \
		pcl-tools \
		python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential python3-catkin-tools\
		ros-$ROS_DISTRO-desktop-full \
		ros-$ROS_DISTRO-ackermann-msgs \
		ros-$ROS_DISTRO-cv-bridge \
		ros-$ROS_DISTRO-fkie-multimaster \
		ros-$ROS_DISTRO-foxglove-bridge \
		ros-$ROS_DISTRO-gps-common \
		ros-$ROS_DISTRO-image-transport-plugins \
		ros-$ROS_DISTRO-navigation \
		ros-$ROS_DISTRO-pcl-conversions \
		ros-$ROS_DISTRO-pcl-ros \
		ros-$ROS_DISTRO-pointcloud-to-laserscan\
		ros-$ROS_DISTRO-robot-localization \
		ros-$ROS_DISTRO-robot-state-publisher ros-$ROS_DISTRO-joint-state-publisher \
		ros-$ROS_DISTRO-tf2-geometry-msgs \
		ros-$ROS_DISTRO-tf-conversions && \
	apt-get clean -qq && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /tmp/* 

# Adding hamster repositories
RUN cd /workspace/src && \
    git clone https://gitlab.com/kjit/hamster/hamster_simulation.git && \
    git clone https://gitlab.com/kjit/hamster/hamster_vehicle_description.git


# ROS workspace build
RUN cd /workspace && \
	source /opt/ros/$ROS_DISTRO/setup.bash && \
	apt update && \
	DEBIAN_FRONTEND=noninteractive rosdep update && \
	DEBIAN_FRONTEND=noninteractive rosdep install --from-paths src --ignore-src -r -y && \
	apt-get clean -qq && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
	catkin build

# Settings of ROS in bashrc
RUN echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> /etc/bash.bashrc && \
	echo "source /workspace/devel/setup.bash" >> /etc/bash.bashrc && \
	echo -e "alias ip4='ip route get 8.8.8.8 | sed -n \"/src/{s/.*src *\([^ ]*\).*/\1/p;q}\"'" >> /etc/bash.bashrc && \
	echo 'export ROS_MASTER_URI=http://$(ip4):11311' >> /etc/bash.bashrc && \
	echo 'export ROS_HOSTNAME=$(ip4)' >> /etc/bash.bashrc && \
	echo 'export ROS_IP=$(ip4)' >> /etc/bash.bashrc
  
# .............................................................................................
## Copy entry.sh into the docker
COPY ./entry.sh /

## Set ownership
RUN chown -R $HOST_UID:$HOST_GID /workspace

#Set user
USER $HOST_USER
WORKDIR /workspace