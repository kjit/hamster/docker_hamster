#! /bin/bash


# docker compose build
export HOST_GID=$(id -g)
export HOST_UID=$(id -u)

# docker compose build
docker build \
    --rm --network=host \
    --build-arg HOST_UID=$HOST_UID \
    --build-arg HOST_GID=$HOST_GID \
    --tag hamster_developer \
    --file Dockerfile .
