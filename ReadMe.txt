## Docker for developing on Hamster

The setup contains a ROS(/workspace) setup.
A terminal window will open with "test" user.
The container uses host networking, means it doesn't own ip address and network layer, it
uses the host machine ip adress and network configuration.

Warning!
If the docker image is rebuilt or any changes are made in the docker-compose file, then the 
earilier used container will be delelted with all of the modification within it.


## Dockerfile
Based on ros:noetic-ros-base (https://hub.docker.com/_/ros/)
Contains basic applications.
Main working folders are created.

## docker-compose.yml
Contains a hamster_developer_container service, that will create the container.

Additional files, folders and volumes of the host PC can be added to the container.
For exmaple:

volumes:
	- /tmp/.X11-unix:/tmp/.X11-unix:rw
	- ./entry.sh:/entry.sh

!!  - source_path:target_path        !!

## entry.sh
It opens the terminal window.
Any additional commands can be added before the "terminator" command, if they are required to 
run before the opening of the terminal window.

## start_client.sh
Start the container based on the docker compose file.
If the docker image is missing, it will build it and automatically start the container.
If the opening terminal window is closed, the container will stop.

## Usage on first time or if modifactions were made in the Dockerfile or in the docker-compose.yml
1. Set the necessary volumes in the docker-compose.yml
2. ./start_client.sh
3. vscode with docker extension can attach to the container and development can be done

## Usage generally
1. Use vscode with docker extension, that can reopen the container and one can continue the work.

